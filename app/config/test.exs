use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :hello, HelloWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :hello, Hello.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "hello_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :hound,
  driver: "chrome_driver",
  host: "http://selenium",
  port: 4444,
  path_prefix: "wd/hub/"
