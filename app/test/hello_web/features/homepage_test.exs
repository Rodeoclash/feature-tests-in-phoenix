defmodule HomepageTest do
  use ExUnit.Case, async: true
  use Hound.Helpers

  hound_session()

  test "the truth", meta do
    {ip, _} = System.cmd("hostname", ["-i"])
    navigate_to("http://#{ip}:4001")
    assert page_title() == "Hello Hello!"
  end
end

defmodule HomepageTest2 do
  use ExUnit.Case, async: true
  use Hound.Helpers

  hound_session()

  test "the false", meta do
    {ip, _} = System.cmd("hostname", ["-i"])
    navigate_to("http://#{ip}:4001")
    refute page_title() == "Hello Hello 2"
  end
end
